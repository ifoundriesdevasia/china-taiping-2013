<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_contact
 *
 * @copyright   Copyright (C) 2005 - 2018 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * Controller for single contact view
 *
 * @since  1.5.19
 */
class ContactControllerContact extends JControllerForm
{
	/**
	 * Method to get a model object, loading it if required.
	 *
	 * @param   string  $name    The model name. Optional.
	 * @param   string  $prefix  The class prefix. Optional.
	 * @param   array   $config  Configuration array for model. Optional.
	 *
	 * @return  JModelLegacy  The model.
	 *
	 * @since   1.6.4
	 */
	public function getModel($name = '', $prefix = '', $config = array('ignore_request' => true))
	{
		return parent::getModel($name, $prefix, array('ignore_request' => false));
	}

	/**
	 * Method to submit the contact form and send an email.
	 *
	 * @return  boolean  True on success sending the email. False on failure.
	 *
	 * @since   1.5.19
	 */
	public function submit()
	{
		// Check for request forgeries.
		$this->checkToken();

		$app    = JFactory::getApplication();
		$model  = $this->getModel('contact');
		$params = JComponentHelper::getParams('com_contact');
		$stub   = $this->input->getString('id');
		$id     = (int) $stub;

		// Get the data from POST
		$data    = $this->input->post->get('jform', array(), 'array');
		$contact = $model->getItem($id);

		$params->merge($contact->params);

		// Check for a valid session cookie
		if ($params->get('validate_session', 0))
		{
			if (JFactory::getSession()->getState() !== 'active')
			{
				JError::raiseWarning(403, JText::_('JLIB_ENVIRONMENT_SESSION_INVALID'));

				// Save the data in the session.
				$app->setUserState('com_contact.contact.data', $data);

				// Redirect back to the contact form.
				$this->setRedirect(JRoute::_('index.php?option=com_contact&view=contact&id=' . $stub, false));

				return false;
			}
		}

		// Contact plugins
		JPluginHelper::importPlugin('contact');
		$dispatcher = JEventDispatcher::getInstance();

		// Validate the posted data.
		$form = $model->getForm();

		if (!$form)
		{
			JError::raiseError(500, $model->getError());

			return false;
		}

		/* if (!$model->validate($form, $data))
		{
			$errors = $model->getErrors();

			foreach ($errors as $error)
			{
				$errorMessage = $error;

				if ($error instanceof Exception)
				{
					$errorMessage = $error->getMessage();
				}

				$app->enqueueMessage($errorMessage, 'error');
			}

			$app->setUserState('com_contact.contact.data', $data);

			$this->setRedirect(JRoute::_('index.php?option=com_contact&view=contact&id=' . $stub, false));

			return false;
		} */
		
		$post = JRequest::get('post');
			
		//if(!$post['no_captcha']) {
		
			JPluginHelper::importPlugin('captcha');
			$dispatcher = JDispatcher::getInstance();
			$code = '';
			if(isset($post['recaptcha_response_field'])) {
				$code = $post['recaptcha_response_field'];
			}
		
			$res = $dispatcher->trigger('onCheckAnswer',$code);
			//echo print_r($res);exit;
			if(!$res[0]){
				// Save the data in the session.
				$app->setUserState('com_contact.contact.data', $data);
				$app->enqueueMessage('Please insert correct Security Code / Captcha', 'warning');
				$this->setRedirect(JRoute::_('index.php?option=com_contact&view=contact&id='.$stub, false));
				return false;
			}
			
		//}

		// Validation succeeded, continue with custom handlers
		$results = $dispatcher->trigger('onValidateContact', array(&$contact, &$data));

		foreach ($results as $result)
		{
			if ($result instanceof Exception)
			{
				return false;
			}
		}

		// Passed Validation: Process the contact plugins to integrate with other applications
		$dispatcher->trigger('onSubmitContact', array(&$contact, &$data));

		// Send the email
		$sent = false;

		if (!$params->get('custom_reply'))
		{
			$sent = $this->_sendEmail($data, $contact, $params->get('show_email_copy', 0));
		}

		// Set the success message if it was a success
		if (!($sent instanceof Exception))
		{
			$msg = JText::_('COM_CONTACT_EMAIL_THANKS');
		}
		else
		{
			$msg = '';
		}

		// Flush the data from the session
		$app->setUserState('com_contact.contact.data', null);

		// Redirect if it is set in the parameters, otherwise redirect back to where we came from
		if ($contact->params->get('redirect'))
		{
			$this->setRedirect($contact->params->get('redirect'), $msg);
		}
		else
		{
			$this->setRedirect(JRoute::_('index.php?option=com_contact&view=contact&id=' . $stub, false), $msg);
		}

		return true;
	}

	/**
	 * Method to get a model object, loading it if required.
	 *
	 * @param   array     $data                  The data to send in the email.
	 * @param   stdClass  $contact               The user information to send the email to
	 * @param   boolean   $copy_email_activated  True to send a copy of the email to the user.
	 *
	 * @return  boolean  True on success sending the email, false on failure.
	 *
	 * @since   1.6.4
	 */
	private function _sendEmail($data, $contact, $copy_email_activated)
	{
		$app = JFactory::getApplication();

		if ($contact->email_to == '' && $contact->user_id != 0)
		{
			$contact_user      = JUser::getInstance($contact->user_id);
			$contact->email_to = $contact_user->get('email');
		}

		//$mailfrom = $app->get('mailfrom');
		$mailfrom = ($contact->email_to)?$contact->email_to:$app->getCfg('mailfrom');
		$fromname	= $app->getCfg('fromname');
		$sitename	= $app->getCfg('sitename');
		$copytext 	= JText::sprintf('COM_CONTACT_COPYTEXT_OF', $contact->name, $sitename);
		
		$layout = JRequest::getVar('layout');
		
		$body = "<div style=\"margin-bottom:20px;\">This mail message was sent from the ".JURI::root()."</div>";
			
		switch($layout) {
			case 'quotation':
				$subject = 'Motor Quotation Request';
				$coverage = $data['contact_coverage'];
				$insurance_period_start = strtotime($data['contact_periodofinsurance_start']);
				$insurance_period_end = strtotime($data['contact_periodofinsurance_end']);
				
				$owner_name = $data['contact_owner_name'];
				$owner_nric = $data['contact_owner_nric'];
				$owner_dob = $data['contact_owner_dob'];
				$owner_passdate = $data['contact_owner_passdate'];
				$owner_gender = $data['contact_owner_gender'];
				$owner_occupation = $data['contact_owner_occupation'];
				$owner_email = $data['contact_owner_email'];
				$owner_contactno = $data['contact_owner_contactno'];
				
				$register_no = $data['contact_registration_no'];
				$register_makemodel = $data['contact_registration_makemodel'];
				$register_year = $data['contact_registration_year'];
				$register_ncd = $data['contact_registration_ncd'];
				$register_offpeakcar = $data['contact_registration_offpeakcar'];
				$register_parallel_import = $data['contact_registration_parallel_import'];
				$claimhistory = nl2br($data['contact_claim_history']);
				
				$body .= '<table width="100%">';
				$body	.= '<tr><td colspan=2><strong>Motor Quotation Detail</td></tr>';
				$body	.= '<tr><td width="150px">Coverage :</td><td>'.$data['contact_coverage'].'</td></tr>';
				$body	.= '<tr><td>Period of Insurance  :</td><td>'.(($insurance_period_start)?date("d M Y",$insurance_period_start):$data['contact_periodofinsurance_start']).' - '.(($insurance_period_start)?date("d M Y",$insurance_period_end):$data['contact_periodofinsurance_end']).'</td></tr>';
				$body	.= '<tr><td colspan=2><strong>Owner\' Particulars</td></tr>';
				$body	.= '<tr><td>Name :</td><td>'.$owner_name.'</td></tr>';
				$body	.= '<tr><td>NRIC :</td><td>'.$owner_nric.'</td></tr>';
				$body	.= '<tr><td>Date of Birth :</td><td>'.$owner_dob.'</td></tr>';
				$body	.= '<tr><td>Pass Date :</td><td>'.$owner_passdate.'</td></tr>';
				$body	.= '<tr><td>Gender :</td><td>'.$owner_gender.'</td></tr>';
				$body	.= '<tr><td>Occupation :</td><td>'.$owner_occupation.'</td></tr>';
				$body	.= '<tr><td>E-mail Address :</td><td>'.$owner_email.'</td></tr>';
				$body	.= '<tr><td>Contact No :</td><td>'.$owner_contactno.'</td></tr>';
				$body	.= '<tr><td colspan=2><strong>Vehicle\'s Particulars</td></tr>';
				$body	.= '<tr><td>Registration No :</td><td>'.$register_no.'</td></tr>';
				$body	.= '<tr><td>Make & Model :</td><td>'.$register_makemodel.'</td></tr>';
				$body	.= '<tr><td>Year of Registration :</td><td>'.$register_year.'</td></tr>';
				$body	.= '<tr><td>NCD :</td><td>'.$register_ncd.'</td></tr>';
				$body	.= '<tr><td>Off-Peak Car :</td><td>'.$register_offpeakcar.'</td></tr>';
				$body	.= '<tr><td>Parallel Import :</td><td>'.$register_parallel_import.'</td></tr>';
				$body	.= '<tr><td colspan=2><strong>Claims History</td></tr>';
				$body	.= '<tr><td>Claim History :</td><td>'.stripslashes($claimhistory).'</td></tr>';
				$body .='</table>';
			
				break;
			case 'business_quotation' :
			
				$subject = 'Business Insurance Quotation Request';
				$contact_person = $data['contact_bus_contactperson'];
				$contact_number = $data['contact_bus_contactno'];
				$contact_email = $data['contact_bus_email'];
				$contact_company_name = $data['contact_bus_company_name'];
				$contact_insurance = explode('|',$data['contact_bus_insurance']);
				$contact_details = nl2br($data['contact_bus_details']);
				
				$body .= '<table width="100%">';
				$body	.= '<tr><td colspan=2><strong>'.$subject.'</td></tr>';
				$body	.= '<tr><td width="150px">Contact Person :</td><td>'.$contact_person.'</td></tr>';
				$body	.= '<tr><td>Contact No. :</td><td>'.$contact_number.'</td></tr>';
				$body	.= '<tr><td>Email Address :</td><td>'.$contact_email.'</td></tr>';
				$body	.= '<tr><td>Insurance :</td><td>'.$contact_insurance[1].'</td></tr>';
				$body	.= '<tr><td>Company Name :</td><td>'.$contact_company_name.'</td></tr>';
				$body	.= '<tr><td>Details :</td><td>'.stripslashes($contact_details).'</td></tr>';
				$body .='</table>';
				
				break;
			default:
				$contact_name		= $data['contact_name'];
				$contact_email		= $data['contact_email'];
				$subject	= $data['contact_subject'];
				$contact_message	= stripslashes(nl2br($data['contact_message']));
				
				// addition on contactus
				$contactno = $data['contact_contactno'];
				
				$body .= '<table width="100%">';
				$body	.= '<tr><td colspan=2><strong>'.$subject.'</td></tr>';
				$body	.= '<tr><td width="150px">Name :</td><td>'.$contact_name.'</td></tr>';
				$body	.= '<tr><td>Email Address :</td><td>'.$contact_email.'</td></tr>';
				$body	.= '<tr><td>Contact No. :</td><td>'.$contactno.'</td></tr>';
				$body	.= '<tr><td>Message :</td><td>'.$contact_message.'</td></tr>';
				$body .='</table>';
				
				// Prepare email body
				//$prefix = JText::sprintf('COM_CONTACT_ENQUIRY_TEXT', JURI::base());
				//$body	= $name.' <'.$email.'>'."\r\n\r\n".'Contact No : '.($data['contact_contactno']?$data['contact_contactno']:'')."\r\n\r\nMessage :\r\n\r\n".stripslashes($body);
			break;
		}

		$mail = JFactory::getMailer();
		$mail->isHTML(true);
		$mail->Encoding = 'base64';
		$mail->addRecipient($contact->email_to);
		$mail->addReplyTo($email, $name);
		$mail->setSender(array($mailfrom, $fromname));
		$mail->setSubject($sitename . ': ' . $subject);
		$mail->setBody($body);
		$sent = $mail->Send();

		// If we are supposed to copy the sender, do so.

		// Check whether email copy function activated
		if ($copy_email_activated == true && !empty($data['contact_email_copy']))
		{
			$copytext    = JText::sprintf('COM_CONTACT_COPYTEXT_OF', $contact->name, $sitename);
			$copytext    .= "\r\n\r\n" . $body;
			$copysubject = JText::sprintf('COM_CONTACT_COPYSUBJECT_OF', $subject);

			$mail = JFactory::getMailer();
			$mail->isHTML(true);
			$mail->Encoding = 'base64';
			$mail->addRecipient($email);
			$mail->addReplyTo($email, $name);
			$mail->setSender(array($mailfrom, $fromname));
			$mail->setSubject($copysubject);
			$mail->setBody($copytext);
			$sent = $mail->Send();
		}

		return $sent;
	}
}
