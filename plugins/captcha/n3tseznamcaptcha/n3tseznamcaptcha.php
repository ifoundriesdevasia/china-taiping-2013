<?php
/**
 * @package n3tSeznamCaptcha
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2012-2013 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.plugin.plugin' );

class plgCaptchaN3tSeznamCaptcha extends JPlugin
{

	public function __construct($subject, $config)
	{
		parent::__construct($subject, $config);
		$this->loadLanguage();
	}
  
	public function onInit($id)
	{
		$doc = JFactory::getDocument();
    if ($this->params->def('theme', 'dark-icons'))
      JHtml::stylesheet('plg_n3tseznamcaptcha/'.$this->params->def('theme', 'dark-icons').'.css', false, true);
    JHtml::script('plg_n3tseznamcaptcha/captcha.js', true, true);
		$doc->addScriptDeclaration("
      window.addEvent('domready', function() {
        new n3tSeznamCaptcha({
          'url': '".JURI::root(true)."',
          'audio': ".( $this->params->def('show_audio', 1) ? "true" : "false" )."
        });
      });"
		);

    JText::script('PLG_CAPTCHA_N3TSEZNAMCAPTCHA_CAPTCHA',true);
    JText::script('PLG_CAPTCHA_N3TSEZNAMCAPTCHA_RELOAD',true);
    JText::script('PLG_CAPTCHA_N3TSEZNAMCAPTCHA_RELOAD_TITLE',true);
    JText::script('PLG_CAPTCHA_N3TSEZNAMCAPTCHA_AUDIO',true);
    JText::script('PLG_CAPTCHA_N3TSEZNAMCAPTCHA_AUDIO_TITLE',true);

    if( !ini_get('allow_url_fopen') && (!extension_loaded('curl') || !function_exists('curl_exec') ) ) {
      throw new Exception(JText::_('PLG_CAPTCHA_N3TSEZNAMCAPTCHA_NO_DOWNLOAD_SUPPORT'));
    }
  }

	public function onDisplay($name, $id, $class)
	{
    return '<div class="seznam-captcha"></div>';
	}

	public function onCheckAnswer($code)
	{
    jimport( 'joomla.filter.input' );
    $requestData = JRequest::getVar('jform', array(), 'post', 'array');
		$hash	= JFilterInput::getInstance()->clean($requestData['captcha_hash'], 'cmd');
		$answer	= JFilterInput::getInstance()->clean($requestData['captcha'], 'cmd');

		if ($hash == null || strlen($hash) == 0 || $answer == null || strlen($answer) == 0)
		{
			$this->_subject->setError(JText::_('PLG_CAPTCHA_N3TSEZNAMCAPTCHA_EMPTY_ANSWER'));
			return false;
		}
    if ($this->_checkAnswer($hash, $answer)) return true;
		$this->_subject->setError(JText::_('PLG_CAPTCHA_N3TSEZNAMCAPTCHA_WRONG_ANSWER'));
		return false;
  }

  private function _checkAnswer($hash, $answer)
  {
    $url = 'http://captcha.seznam.cz/captcha.check?hash='.$hash.'&code='.$answer;
    if (extension_loaded('curl') && function_exists('curl_exec')) {
      $c = curl_init();
      curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($c, CURLOPT_URL, $url);
      curl_exec($c);
      $info = curl_getinfo($c);
      curl_close($c);
      return $info['http_code'] == 200;
    } else if (ini_get('allow_url_fopen')) {
      $headers = get_headers($url);
      if (is_array($headers)
      && isset($headers[0])
      && strpos($headers[0],'200'))
        return true;
    }
    return false;
  }
}