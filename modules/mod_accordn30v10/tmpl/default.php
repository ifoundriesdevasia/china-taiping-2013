<?php
/**
 * @package      mod_accordion30v10
 * @subpackage Modules
 * @link www.blackdale.com
 * @Copyright Bob Galway</copyright>
 * @license>GPL3   http://www.gnu.org/licenses/
 */

// no direct access
defined('_JEXEC') or die;

//Collect Parameters

$headercol=$params->get('headercol');
$headercolbg=$params->get('headercolbg');
$tag=$params->get('tag');
$scripts=$params->get('scripts');
$scriptuse=$params->get('scriptuse');

$item1=$params->get('item1');
$item2=$params->get('item2');
$item3=$params->get('item3');
$item4=$params->get('item4');
$item5=$params->get('item5');
$item6=$params->get('item6');
$item7=$params->get('item7');
$item8=$params->get('item8');


$title1=$params->get('title1');
$title2=$params->get('title2');
$title3=$params->get('title3');
$title4=$params->get('title4');
$title5=$params->get('title5');
$title6=$params->get('title6');
$title7=$params->get('title7');
$title8=$params->get('title8');

$code1=$params->get('code1');
$code2=$params->get('code2');
$code3=$params->get('code3');
$code4=$params->get('code4');
$code5=$params->get('code5');
$code6=$params->get('code6');
$code7=$params->get('code7');
$code8=$params->get('code8');
$paddingleft=$params->get('paddingleft');
$paddingright=$params->get('paddingright');
$paddingtop=$params->get('paddingtop');
$paddingbottom=$params->get('paddingbottom');
$margintop=$params->get('margin-top');
$marginbottom=$params->get('margin-bottom');
$marginleftmodule=$params->get('margin-leftmodule');

$width=$params->get('width');
$widthunit=$params->get('widthunit');

$colour2=$params->get('colour2');
$action=$params->get('action');
$modno=$params->get('modno');
if ($modno==0){$modno=($module->id);}
$display=$params->get('display');

$cssuse=$params->get('cssuse');
$ac_css=$params->get('ac_css');

// enable scripts
if ($scriptuse==1){
$doc =& JFactory::getDocument();
$doc->addCustomTag( $scripts );

}
//Set up accordion

if ($action==1){
	$script='
window.addEvent("domready", function() {
    var accordion_'.$modno.' = new Fx.Accordion($$(".toggler_'.$modno.'"),$$(".content_'.$modno.'"), {
        opacity: 0,
 		alwaysHide:true,
		'.$display.'
        onActive: function(toggler_'.$modno.') {

                     toggler_'.$modno.'.setStyle("color", "'.$headercol.'");
                     toggler_'.$modno.'.setStyle("cursor", "pointer");
 },

        onBackground: function(toggler_'.$modno.') {

                      toggler_'.$modno.'.setStyle("color", "'.$headercolbg.'");
                      toggler_'.$modno.'.setStyle("cursor", "pointer");
 }
    });

$$(".toggler_'.$modno.'").addEvent("mouseenter",function(){this.fireEvent("click" );});});


';
}
else if ($action==2){
	$script='

window.addEvent("domready", function() {
    var accordion_'.$modno.' = new Fx.Accordion($$(".toggler_'.$modno.'"),$$(".content_'.$modno.'"), {
        opacity: 0,
 		alwaysHide:true,
		'.$display.'
        onActive: function(toggler_'.$modno.') {

                     toggler_'.$modno.'.setStyle("color", "'.$headercol.'");
                     toggler_'.$modno.'.setStyle("cursor", "pointer");
 },

        onBackground: function(toggler_'.$modno.') {

                      toggler_'.$modno.'.setStyle("color", "'.$headercolbg.'");
                      toggler_'.$modno.'.setStyle("cursor", "pointer");
 }
    });

$$(".toggler_'.$modno.'").addEvent("click" );

});';

}

$css_ac='#accordion_'.$modno.'{overflow:hidden;width:'.$width.$widthunit.';';
if(!empty($paddingright)){$css_ac.='padding-right:'.$paddingright.'px;';}
if(!empty($paddingleft)){$css_ac.='padding-left:'.$paddingleft.'px;';}
if(!empty($paddingbottom)){$css_ac.='padding-bottom:'.$paddingbottom.'px;';}
if(!empty($paddingtop)){$css_ac.='padding-top:'.$paddingtop.'px;';}
if(!empty($marginleftmodule)){$css_ac.='margin-left:'.$marginleftmodule.'px;';}
if(!empty($margintop)){$css_ac.='margin-top:'.$margintop.';px';}
if(!empty($marginbottom)){$css_ac.='margin-bottom:'.$marginbottom.'px;';}
if(!empty($colour2)){$css_ac.='background-color:'.$colour2.';';}
$css_ac.='}';

$doc =&JFactory::getDocument();
$doc->addScriptDeclaration($script,'text/javascript');
$doc->addStyleDeclaration($css_ac,'text/css');
//Place user snippets in accordion structure

echo '<div id="accordion_'.$modno.'">';


if($item1==1){
echo '

	<'.$tag.' class="toggler_'.$modno.' " style="cursor:pointer;color:'.$headercolbg.';">'.$title1.'</'.$tag.'>
	<div class="content_'.$modno.' ">
		'.$code1.'
		</div>';

 }

 if($item2==1){
echo '

	<'.$tag.' class="toggler_'.$modno.' " style="cursor:pointer;color:'.$headercolbg.';">'.$title2.'</'.$tag.'>
	<div class="content_'.$modno.' ">

		'.$code2.'

	</div>';
 }

if($item3==1){
echo '

	<'.$tag.' class="toggler_'.$modno.' " style="cursor:pointer;color:'.$headercolbg.';">'.$title3.'</'.$tag.'>
	<div class="content_'.$modno.' ">

		'.$code3.'

	</div>';
 }

if($item4==1){
echo '
	<'.$tag.' class="toggler_'.$modno.' " style="cursor:pointer;color:'.$headercolbg.';" >'.$title4.'</'.$tag.'>
	<div class="content_'.$modno.' ">

		'.$code4.'
	</div>';
	}



if($item5==1){
echo '


<'.$tag.' class="toggler_'.$modno.' " style="cursor:pointer;color:'.$headercolbg.';">'.$title5.'</'.$tag.'>
	<div class="content_'.$modno.' ">

		'.$code5.'
	</div>';

	}


	if($item6==1){
echo '

<'.$tag.' class="toggler_'.$modno.' " style="cursor:pointer;color:'.$headercolbg.';">'.$title6.'</'.$tag.'>
	<div class="content_'.$modno.' ">

		'.$code6.'
	</div>';
}

	if($item7==1){
echo '

<'.$tag.' class="toggler_'.$modno.' " style="cursor:pointer;color:'.$headercolbg.';" >'.$title7.'</'.$tag.'>
	<div class="content_'.$modno.' ">

		'.$code7.'
	</div>';
}

	if($item8==1){
echo '

<'.$tag.' class="toggler_'.$modno.' " style="cursor:pointer;color:'.$headercolbg.';" >'.$title8.'</'.$tag.'>
	<div class="content_'.$modno.' ">

		'.$code8.'
	</div>';
}


echo '</div>';


 ?>

