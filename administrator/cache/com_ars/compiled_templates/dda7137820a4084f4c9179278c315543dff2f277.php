<?php
/**
 * @package   AkeebaReleaseSystem
 * @copyright Copyright (c)2010-2018 Nicholas K. Dionysopoulos / Akeeba Ltd
 * @license   GNU General Public License version 3, or later
 */

defined('_JEXEC') or die;
?>
<?php $this->startSection('footer'); ?>
	<p style="font-size: small" class="akeeba-panel--information">
		<strong>
			Akeeba Release System &bull;
			<?php echo \JText::sprintf('COM_ARS_CPANEL_COPYRIGHT_LABEL', date('Y')); ?>
		</strong>
		<br />

		<?php echo \JText::_('COM_ARS_CPANEL_LICENSE_LABEL'); ?>
	</p>
<?php $this->stopSection(); ?>
