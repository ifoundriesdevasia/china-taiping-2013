<?php
/**
 * @package   AkeebaReleaseSystem
 * @copyright Copyright (c)2010-2018 Nicholas K. Dionysopoulos / Akeeba Ltd
 * @license   GNU General Public License version 3, or later
 */

/** @var  \Akeeba\ReleaseSystem\Admin\View\ControlPanel\Html  $this */

defined('_JEXEC') or die;
?>

<?php $this->startSection('phpVersionWarning'); ?>
    <?php if(version_compare(PHP_VERSION, '5.5.0', 'lt')): ?>
    <div id="phpVersionCheck" class="alert alert-warning">
        <h3>
            <?php echo \JText::_('AKEEBA_COMMON_PHPVERSIONTOOOLD_WARNING_TITLE'); ?>
        </h3>
        <p>
            <?php echo \JText::sprintf('AKEEBA_COMMON_PHPVERSIONTOOOLD_WARNING_BODY',
                PHP_VERSION,
                $this->akeebaCommonDatePHP,
                $this->akeebaCommonDateObsolescence,
                '5.5'
            ); ?>
        </p>
    </div>
    <?php endif; ?>
<?php $this->stopSection(); ?>
