<?php
/**
 * @package   AkeebaReleaseSystem
 * @copyright Copyright (c)2010-2018 Nicholas K. Dionysopoulos / Akeeba Ltd
 * @license   GNU General Public License version 3, or later
 */

/** @var  \Akeeba\ReleaseSystem\Admin\View\ControlPanel\Html  $this */

defined('_JEXEC') or die;
?>
<?php $this->addCssFile('media://com_ars/css/backend.css'); ?>
<?php $this->addJavascriptFile('media://com_ars/js/gui-helpers.js'); ?>

<?php echo \JHtml::_('behavior.core'); ?>
<?php echo \JHtml::_('formbehavior.chosen', 'select'); ?>

<?php /* Include external sections. Do note how you can include sub-templates in one order and compile them in a completely
different order using <?php echo $this->yieldContent; ?> later on! */ ?>
<?php echo $this->loadAnyTemplate('admin:com_ars/ControlPanel/footer'); ?>
<?php echo $this->loadAnyTemplate('admin:com_ars/ControlPanel/graphs'); ?>
<?php echo $this->loadAnyTemplate('admin:com_ars/ControlPanel/icons_compat'); ?>
<?php echo $this->loadAnyTemplate('admin:com_ars/ControlPanel/phpversion'); ?>

<?php /* Note: I don't pass $this->hasGeoIPPlugin and $this->geoIPPluginNeedsUpdate. This demonstrates how Blade
subtemplates can view their parent's variables automatically. */ ?>
<?php echo $this->loadAnyTemplate('admin:com_ars/ControlPanel/geoip'); ?>

<?php /* Compile the output. Do note that I don't need to wrap it in a section. Content outside a section is yielded
immediately. Alternatively I could wrap this in a <?php $this->startSection; ?>/<?php echo $this->yieldSection(); ?> block or even <?php $this->startSection; ?>/<?php $this->stopSection(); ?> and use <?php echo $this->yieldContent; ?> to
render it. */ ?>
<?php echo $this->yieldContent('phpVersionWarning', ''); ?>

<?php if($this->needsMenuItem): ?>
<div class="akeeba-block--info">
	<h4>
		<?php echo \JText::_('COM_ARS_MISSING_CATEGORIES_MENU_HEAD'); ?>
	</h4>
	<?php echo \JText::_('COM_ARS_MISSING_CATEGORIES_MENU'); ?>
</div>
<?php endif; ?>

<?php /* This DIV is required to render the update notification, if there is an update available */ ?>
<div id="updateNotice"></div>

<?php echo $this->yieldContent('geoip', ''); ?>

<div class="akeeba-container--50-50">
	<div>
		<?php echo $this->yieldContent('graphs'); ?>
	</div>
	<div>
		<?php echo $this->yieldContent('icons'); ?>
	</div>
</div>

<div>
	<?php echo $this->yieldContent('footer'); ?>
</div>
