<?php
	/**
		* @package     Joomla.Site
		* @subpackage  com_contact
		*
		* @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
		* @license     GNU General Public License version 2 or later; see LICENSE.txt
	*/
	
	defined('_JEXEC') or die;
	JHtml::_('behavior.keepalive');
	JHtml::_('behavior.formvalidation');
	JHtml::_('behavior.tooltip');


if (isset($this->error)) : ?>
<div class="contact-error">
	<?php echo $this->error; ?>
</div>
<?php endif; ?>
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript">
	

	function clearContactUsForm(){
		jQuery('.control-group input').val('');
		jQuery('textarea').val('');
	}
	function checkEmailAddress(thisinput) {
		var value = thisinput.value;
		var emailRegEx = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;
		if (value.search(emailRegEx) == -1) {
			jQuery('#' + thisinput.id).val('');
			alert("Please enter a valid email address.");
		}
		
	}
	
	function checkStartDate(thisinput) {
		
		var start_date = thisinput.value;
		var sd = start_date.split('-');
		
		var year1= parseInt(sd[2],10);
		var month1= parseInt(sd[1],10);
		var day1= parseInt(sd[0]);
		
		jQuery('#jform_contact_periodofinsurance_end').val('');
		
		if(!checkDateFormat(thisinput)) {
			return false;
		}
		
		var todaydate = Math.round(new Date().getTime() / 1000);
		var inputdate = Math.round(new Date(year1,(month1-1),(day1+1),0,0,0,0).getTime() / 1000);

		
		if(inputdate <= todaydate) {
			jQuery('#'+thisinput.id).val('');
			alert('You cannot choose earlier than today\'s date');
			return;
		}
		
		// must be 1 year - 1 day
		var tmps = new Date((year1+1),(month1-1),(day1-1),0,0,0,0);
		
		var year1= parseInt(tmps.getFullYear());
		var month1= parseInt(tmps.getMonth())+1;
		var day1= parseInt(tmps.getDate());
		
		if(day1<10){day1='0'+day1}
		if(month1<10){month1='0'+month1}
		
		var enddate = day1+'-'+month1+'-'+year1;
		
		if(!isValidDate2(enddate)) {
			jQuery('#jform_contact_periodofinsurance_end').val('');
			alert("End Date is not Valid");
			return false;
		}
		
		jQuery('#jform_contact_periodofinsurance_end').val(enddate);
		
		
	}
	function checkEndDate(thisinput) {
		
		// check if more than 1 year from startdate
		var start_date = jQuery('#jform_contact_periodofinsurance_start').val();
		var end_date = thisinput.value;
		
		var sd = start_date.split('-');
		var ed = end_date.split('-');
		
		var year1= parseInt(sd[2]);
		var month1= parseInt(sd[1],10);
		var day1= parseInt(sd[0],10);
		
		var year2= parseInt(ed[2]);
		var month2= parseInt(ed[1],10);
		var day2= parseInt(ed[0],10);
		
		var start_date = new Date(year1, month1, day1); 
		var end_date_in_one_year = new Date( (year1+1), month1-1, day1-1);
		var end_date = new Date(year2, month2-1, day2);
		
		if(!checkDateFormat(thisinput)) {
			return false;
		}
		
		if(end_date_in_one_year > end_date) {
			jQuery('#'+thisinput.id).val('');
			alert("The range must be at least 1 year apart.");
			return false;
		}
		
	}
	
	function checkDateFormat(s) {
		
		if(!isValidDate2(s.value)){
			jQuery('#'+s.id).val('');
			//alert("The Date is not Valid");
			return false;
		}
		return true;
	}
	
	function isValidDate2(s) {
		var bits = s.split('-');
		var y = bits[2], m  = bits[1], d = bits[0];
		// Assume not leap year by default (note zero index for Jan)
		var daysInMonth = [31,28,31,30,31,30,31,31,30,31,30,31];
		
		// If evenly divisible by 4 and not evenly divisible by 100,
		// or is evenly divisible by 400, then a leap year
		if ( (!(y % 4) && y % 100) || !(y % 400)) {
			daysInMonth[1] = 29;
		}
		return d <= daysInMonth[--m];
	}
	
	function checkYear(tmp) {
		var years = parseInt(tmp.value);
		
		var nowd = new Date();
		var current_year = parseInt(nowd.getFullYear());
		
		if(current_year < years) {
			alert('The year you insert cannot be later than current year');
			jQuery('#'+tmp.id).val('');
		}
		
	}
	
	function checkPastDate(tmp) {
		var thisdate = tmp.value;
		var nowd = new Date();
		
		var sd = thisdate.split('-');
		
		var year1 = parseInt(sd[2]);
		var month1 = parseInt(sd[1],10);
		var day1 = parseInt(sd[0],10);
		
		var thisdate = new Date(year1, month1-1, day1); 
		
		if(nowd.getTime() < thisdate.getTime()) {
			alert('The date cannot be later than current date');
			jQuery('#'+tmp.id).val('');
		}
	}
	
	function checkDateDashed(e, thisinput) {
		var ltext = thisinput.value.length;
		var returns = thisinput.value;
		
		if(e.which == 8) { //backspace
			return;
		}
		if(ltext == 1 || ltext == 2 || ltext == 4 || ltext == 5 || ltext ==7 || ltext == 8 || ltext == 9 || ltext == 10) {
			if(e.which < 48 || e.which > 57) { // if not 0-9
				returns = thisinput.value.substring(0, ltext - 1);
			}
		}
		
		if(ltext == 3 || ltext == 6) {
			if(e.which != 173) { // if not '-'
				returns = thisinput.value.substring(0, ltext - 1);
			}
		}

		jQuery('#' + thisinput.id).val(returns);
	}
	
	jQuery( document ).ready(function($) {
		
		$('#jform_contact_owner_contactno').keyup(function () { 
			this.value = this.value.replace(/[^0-9\.]/g,'');
		});
		
		
		$('#jform_contact_registration_year').keyup(function () { 
			this.value = this.value.replace(/[^0-9\.]/g,'');
		});
		
		
		$( "#jform_contact_periodofinsurance_start" ).datepicker({
			changeMonth: true,
			changeYear: true,
			yearRange: "c-0:c+20",
			minDate : 0,
			dateFormat : "dd-mm-yy"
		});
		
		$( "#jform_contact_periodofinsurance_end" ).datepicker({
			changeMonth: true,
			changeYear: true,
			yearRange: "c-0:c+20",
			minDate : 0,
			dateFormat : "dd-mm-yy"
		});
		
		
		$( "#jform_contact_owner_dob" ).datepicker({
			changeMonth: true,
			changeYear: true,
			yearRange: "c-100:c+0",
			maxDate : 0,
			dateFormat : "dd-mm-yy"
		});
		
		$( "#jform_contact_owner_passdate" ).datepicker({
			changeMonth: true,
			changeYear: true,
			yearRange: "c-50:c+0",
			maxDate : 0,
			dateFormat : "dd-mm-yy"
		});
	});

</script>

<div class="quotation-form">
	<form id="quotation-form" action="<?php echo JRoute::_('index.php'); ?>" method="post" class="form-validate form-horizontal">
		<fieldset>
			<?php /*<legend><?php echo JText::_('COM_CONTACT_FORM_LABEL'); ?></legend>*/ ?>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('contact_coverage'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('contact_coverage'); ?></div>
				<div class="clear"></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('contact_periodofinsurance'); ?></div>
				<div class="controls">
					<div style="float:left;margin-right:20px;width:150px;">From : <?php echo $this->form->getInput('contact_periodofinsurance_start'); ?><br />DD-MM-YYYY</div>
					<div style="float:left;width:150px;">To : <?php echo $this->form->getInput('contact_periodofinsurance_end'); ?><br />DD-MM-YYYY</div>
					<div class="clear"></div>
				</div>
				<div class="clear"></div>
			</div>
			
			<h2>Owner's Particulars</h2>
			
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('contact_owner_name'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('contact_owner_name'); ?></div>
				<div class="clear"></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('contact_owner_nric'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('contact_owner_nric'); ?></div>
				<div class="clear"></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('contact_owner_dob'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('contact_owner_dob'); ?><br />DD-MM-YYYY</div>
				<div class="clear"></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('contact_owner_passdate'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('contact_owner_passdate'); ?><br />DD-MM-YYYY</div>
				<div class="clear"></div>
			</div>
			
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('contact_owner_gender'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('contact_owner_gender'); ?></div>
				<div class="clear"></div>
			</div>
			
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('contact_owner_occupation'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('contact_owner_occupation'); ?></div>
				<div class="clear"></div>
			</div>
			
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('contact_owner_email'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('contact_owner_email'); ?></div>
				<div class="clear"></div>
			</div>
			
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('contact_owner_contactno'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('contact_owner_contactno'); ?></div>
				<div class="clear"></div>
			</div>
			
			<h2>Vehicle's Particulars</h2>
			
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('contact_registration_no'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('contact_registration_no'); ?></div>
				<div class="clear"></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('contact_registration_makemodel'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('contact_registration_makemodel'); ?><br />E.g: Volkswagen Passat 2.0 (A), Mercedes Benz C180 Kompressor</div>
				<div class="clear"></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('contact_registration_year'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('contact_registration_year'); ?></div>
				<div class="clear"></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('contact_registration_ncd'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('contact_registration_ncd'); ?></div>
				<div class="clear"></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('contact_registration_offpeakcar'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('contact_registration_offpeakcar'); ?></div>
				<div class="clear"></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('contact_registration_parallel_import'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('contact_registration_parallel_import'); ?></div>
				<div class="clear"></div>
			</div>
			
			<h2>Claims History (Past 3 Years)<br />(Please declare in details, e.g. date of accident, claim amount, paid and/or reserve for Own Damage Claim and/or Third Party claim and/or Third Party Injury claim)</h2>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('contact_claim_history'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('contact_claim_history'); ?></div>
			</div>
			<div class="clear"></div>
			
			<?php 	if ($this->params->get('show_email_copy')){ ?>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('contact_email_copy'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('contact_email_copy'); ?></div>
				</div>
			<?php 	} ?>
			
			<!--<input type="hidden" name="no_captcha" value="1" />-->
			<?php //Dynamically load any additional fields from plugins. ?>
			<?php foreach ($this->form->getFieldsets() as $fieldset) : ?>
				<?php if ($fieldset->name != 'contact'):?>
					<?php $fields = $this->form->getFieldset($fieldset->name);?>
					<?php foreach ($fields as $field) : ?>
						<div class="control-group" style="margin-top:20px;">
							<?php if ($field->hidden) : ?>
								<div class="controls">
									<?php echo $field->input;?><div class="clear"></div>
								</div>
							<?php else:?>
								<div class="control-label">
									<?php echo $field->label; ?>
									<?php if (!$field->required && $field->type != "Spacer") : ?>
										<span class="optional"><?php echo JText::_('COM_CONTACT_OPTIONAL');?></span>
									<?php endif; ?>
								</div>
								<div class="controls"><?php echo $field->input;?><div class="clear"></div></div>
							<?php endif;?>
						</div>
						<div class="clear"></div>
					<?php endforeach;?>
				<?php endif ?>
			<?php endforeach; ?>
			
			<div class="control-group" style="margin-top:20px;">
				
				<div class="controls all-buttons" >
					<button class="bluebutton" type="button" onclick="javascript:clearContactUsForm();"><?php echo JText::_('COM_CONTACT_CONTACT_CLEAR'); ?></button>
					&nbsp;&nbsp;&nbsp;&nbsp;
					<button class="bluebutton validate" type="submit"><?php echo JText::_('COM_CONTACT_CONTACT_SUBMIT'); ?></button>
				</div>
			</div>
			
			<div class="form-actions">
				
				<input type="hidden" name="option" value="com_contact" />
				<input type="hidden" name="task" value="contact.submit" />
				<input type="hidden" name="return" value="<?php echo $this->return_page;?>" />
				<input type="hidden" name="id" value="<?php echo $this->contact->slug; ?>" />
				<input type="hidden" name="layout" value="quotation" />
				<?php echo JHtml::_('form.token'); ?>
			</div>
		</fieldset>
	</form>
</div>
