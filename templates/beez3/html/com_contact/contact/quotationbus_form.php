<?php
	/**
		* @package     Joomla.Site
		* @subpackage  com_contact
		*
		* @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
		* @license     GNU General Public License version 2 or later; see LICENSE.txt
	*/
	
	defined('_JEXEC') or die;
	JHtml::_('behavior.keepalive');
	JHtml::_('behavior.formvalidation');
	JHtml::_('behavior.tooltip');
	
	$db = JFactory::getDBO();
	
	$query = " SELECT id,alias FROM #__contact_details WHERE alias LIKE 'e-%' ";
	$db->setQuery( $query );
	$buscontact = $db->loadObjectList();
	
if (isset($this->error)) : ?>
<div class="contact-error">
	<?php echo $this->error; ?>
</div>
<?php endif; ?>
<script type="text/javascript">
	
	function clearContactUsForm(){
		jQuery('.control-group input').val('');
		jQuery('textarea').val('');
	}
	
	var motr = [];
	
	<?php foreach($buscontact as $v) : ?>
	motr.push({key: '<?php echo $v->alias ?>', value:  '<?php echo $v->id ?>:<?php echo $v->alias ?>'});
	<?php endforeach; ?>
	
	jQuery(document).ready(function() {
		jQuery('#jform_contact_bus_insurance').change(function(){
			var value = jQuery(this).val();
			var items = value.split("|"); // type , business insurance name
			
			var result = '';
			
			for(i in motr) {
				if(items[0] == motr[i].key) {
					result = motr[i].value;
				}
			}
			
			jQuery('#contact_id').val(result);
		});
	});
</script>

<div class="quotation-form">
	<form id="quotation-form" action="<?php echo JRoute::_('index.php'); ?>" method="post" class="form-validate form-horizontal">
		<fieldset>
			<?php /*<legend><?php echo JText::_('COM_CONTACT_FORM_LABEL'); ?></legend>*/ ?>
			<div class="control-group">
			<div class="control-label"><?php echo $this->form->getLabel('contact_bus_contactperson'); ?></div>
			<div class="controls"><?php echo $this->form->getInput('contact_bus_contactperson'); ?></div>
			<div class="clear"></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('contact_bus_contactno'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('contact_bus_contactno'); ?></div>
				<div class="clear"></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('contact_bus_email'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('contact_bus_email'); ?></div>
				<div class="clear"></div>
			</div>
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('contact_bus_insurance'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('contact_bus_insurance'); ?></div>
				<div class="clear"></div>
			</div>
			
			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('contact_bus_company_name'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('contact_bus_company_name'); ?></div>
				<div class="clear"></div>
			</div>

			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('contact_bus_details'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('contact_bus_details'); ?></div>
				<div class="clear"></div>
			</div>
			<div class="clear"></div>

			<?php 	if ($this->params->get('show_email_copy')){ ?>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('contact_email_copy'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('contact_email_copy'); ?></div>
				</div>
			<?php 	} ?>
			<!--<input type="hidden" name="no_captcha" value="1" />-->
			<?php //Dynamically load any additional fields from plugins. ?>
			<?php foreach ($this->form->getFieldsets() as $fieldset) : ?>
				<?php if ($fieldset->name != 'contact'):?>
					<?php $fields = $this->form->getFieldset($fieldset->name);?>
					<?php foreach ($fields as $field) : ?>
						<div class="control-group" style="margin-top:20px;">
							<?php if ($field->hidden) : ?>
								<div class="controls">
									<?php echo $field->input;?><div class="clear"></div>
								</div>
							<?php else:?>
								<div class="control-label">
									<?php echo $field->label; ?>
									<?php if (!$field->required && $field->type != "Spacer") : ?>
										<span class="optional"><?php echo JText::_('COM_CONTACT_OPTIONAL');?></span>
									<?php endif; ?>
								</div>
								<div class="controls"><?php echo $field->input;?><div class="clear"></div></div>
							<?php endif;?>
						</div>
						<div class="clear"></div>
					<?php endforeach;?>
				<?php endif ?>
			<?php endforeach; ?>
			
			<div class="control-group" style="margin-top:20px;">
				
				<div class="controls all-buttons" >
					<button class="bluebutton" type="button" onclick="javascript:clearContactUsForm();"><?php echo JText::_('COM_CONTACT_CONTACT_CLEAR'); ?></button>
					&nbsp;&nbsp;&nbsp;&nbsp;
					<button class="bluebutton validate" type="submit"><?php echo JText::_('COM_CONTACT_CONTACT_SUBMIT'); ?></button>
				</div>
			</div>
			
			<div class="form-actions">
				
				<input type="hidden" name="option" value="com_contact" />
				<input type="hidden" name="task" value="contact.submit" />
				<input type="hidden" name="return" value="<?php echo $this->return_page;?>" />
				<input type="hidden" id="contact_id" name="id" value="<?php echo $this->contact->slug; ?>" />
				<input type="hidden" name="layout" value="business_quotation" />
				<?php echo JHtml::_('form.token'); ?>
			</div>
		</fieldset>
	</form>
</div>
