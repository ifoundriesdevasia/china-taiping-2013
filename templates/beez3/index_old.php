<?php
	/**
		* @package     Joomla.Site
		* @subpackage  Templates.beez3
		* @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
		* @license     GNU General Public License version 2 or later; see LICENSE.txt
	*/
	
	// No direct access.
	defined('_JEXEC') or die;
	
	JLoader::import('joomla.filesystem.file');
	
	
	// Check modules
	$showRightColumn	= ($this->countModules('position-3') or $this->countModules('position-6') or $this->countModules('position-8'));
	$showbottom			= ($this->countModules('position-9') or $this->countModules('position-10') or $this->countModules('position-11'));
	$showleft			= ($this->countModules('position-4') or $this->countModules('position-7') or $this->countModules('position-5'));
	
	if ($showRightColumn == 0 and $showleft == 0)
	{
		$showno = 0;
	}
	
	JHtml::_('behavior.framework', true);
	//echo JURI::root();
	// Get params
	$color				= $this->params->get('templatecolor');
	$logo				= $this->params->get('logo');
	$navposition		= $this->params->get('navposition');
	$headerImage		= $this->params->get('headerImage');
	$app				= JFactory::getApplication();
	$doc				= JFactory::getDocument();
	$templateparams		= $app->getTemplate(true)->params;
	$config = JFactory::getConfig();
	
	$bootstrap = explode(',', $templateparams->get('bootstrap'));
	$jinput = JFactory::getApplication()->input;
	$option = $jinput->get('option', '', 'cmd');
	
	$sitename = $app->getCfg('sitename');
	
	if (in_array($option, $bootstrap))
	{
		// Load optional rtl Bootstrap css and Bootstrap bugfixes
		JHtml::_('bootstrap.loadCss', true, $this->direction);
	}
	
	$doc->addStyleSheet(JURI::base() . 'templates/system/css/system.css');
	$doc->addStyleSheet(JURI::base() . 'templates/' . $this->template . '/css/position.css', $type = 'text/css', $media = 'screen,projection');
	$doc->addStyleSheet(JURI::base() . 'templates/' . $this->template . '/css/layout.css', $type = 'text/css', $media = 'screen,projection');
	$doc->addStyleSheet(JURI::base() . 'templates/' . $this->template . '/css/print.css', $type = 'text/css', $media = 'print');
	$doc->addStyleSheet(JURI::base() . 'templates/' . $this->template . '/css/general.css', $type = 'text/css', $media = 'screen,projection');
	$doc->addStyleSheet(JURI::base() . 'templates/' . $this->template . '/css/' . htmlspecialchars($color) . '.css', $type = 'text/css', $media = 'screen,projection');
	
	/*if ($this->direction == 'rtl')
		{
		$doc->addStyleSheet($this->baseurl . '/templates/' . $this->template . '/css/template_rtl.css');
		if (file_exists(JPATH_SITE . '/templates/' . $this->template . '/css/' . $color . '_rtl.css'))
		{
		$doc->addStyleSheet($this->baseurl . '/templates/' . $this->template . '/css/' . htmlspecialchars($color) . '_rtl.css');
		}
		}
	*/
	
	JHtml::_('bootstrap.framework');
	//$doc->addScript($this->baseurl . '/templates/' . $this->template . '/javascript/md_stylechanger.js', 'text/javascript');
	//$doc->addScript($this->baseurl . '/templates/' . $this->template . '/javascript/hide.js', 'text/javascript');
	//$doc->addScript($this->baseurl . '/templates/' . $this->template . '/javascript/respond.src.js', 'text/javascript');
	
?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>" >
	<head>
		<?php require __DIR__ . '/jsstrings.php';?>
		
		<meta name="viewport" content="width=device-width,initial-scale=1, user-scalable=no "/>
		<!-- height=device-height, initial-scale=1.0, maximum-scale=3.0,user-scalable=no -->
		<!-- maximum-scale= 0.8, user-scalable=yes -->
		
		<meta name="HandheldFriendly" content="true" />
		<meta name="apple-mobile-web-app-capable" content="yes" />
		
		<jdoc:include type="head" />
		
		<!--[if lte IE 8]>
			<link href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/css/ieonly.css" rel="stylesheet" type="text/css" />
		<![endif]-->
		
		<!--[if IE 7]>
			<link href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/css/ie7only.css" rel="stylesheet" type="text/css" />
		<![endif]-->        
        
		
		<script type="text/javascript">
			
			jQuery(document).ready(function() {
				var q = jQuery("ul#i-want-to-menu").width()-2;
				jQuery("ul#i-want-to-menu li").css("width", q);
				
				/* megamenu */
				/* index */
				/*
					0 = Personal Insurance
					1 = Business Insurance
					2 = Claims
					3 = Resource Center
					4 = About Us
				*/
				/*jQuery(".megamenu-wrapper").mouseout(function(){
					jQuery(this).css('display','none');
					});
				*/
				
				var width = jQuery(window).width();
				
				if(width <= 959 && width >= 768) {
					jQuery("ul#taiping-menu li a").each(function(index) {
						jQuery(this).mouseover(function(){
							jQuery('.megamenu-wrapper').css('display','none');
							jQuery('#megamenu-arrow').css('display','none');
							switch(index) {
								case 0:
								jQuery('#personal-insurance-megamenu').css('display','block');
								jQuery('#megamenu-arrow').css('display','block');
								jQuery('#megamenu-arrow').css('left','75px');
								break;
								case 1:
								jQuery('#business-insurance-megamenu').css('display','block');
								jQuery('#megamenu-arrow').css('display','block');
								jQuery('#megamenu-arrow').css('left','240px');
								break;
								case 2:
								jQuery('#claims-megamenu').css('display','block');
								jQuery('#megamenu-arrow').css('display','block');
								jQuery('#megamenu-arrow').css('left','365px');
								break;
								case 3:
								jQuery('#resource-center-megamenu').css('display','block');
								jQuery('#megamenu-arrow').css('display','block');
								jQuery('#megamenu-arrow').css('left','480px');
								break;
								case 4:
								jQuery('#about-us-megamenu').css('display','block');
								jQuery('#megamenu-arrow').css('display','block');
								jQuery('#megamenu-arrow').css('left','590px');
								break;
							}
						});
					});
				}
				
				if(width < 768) {
					jQuery('#homepage-banner-for-mobile').css('display','');
					jQuery('#homepage-banner-for-big').css('display','none');
				} else {
					jQuery('#homepage-banner-for-mobile').css('display','none');
					jQuery('#homepage-banner-for-big').css('display','');
				}
				
				jQuery(window).resize(function(){
					if(jQuery(this).width() != width){
						width = jQuery(this).width();
						
						if(width <= 959 && width >= 768) {
							jQuery("ul#taiping-menu li a").each(function(index) {
								jQuery(this).mouseover(function(){
									jQuery('.megamenu-wrapper').css('display','none');
									jQuery('#megamenu-arrow').css('display','none');
									switch(index) {
										case 0:
										jQuery('#personal-insurance-megamenu').css('display','block');
										jQuery('#megamenu-arrow').css('display','block');
										jQuery('#megamenu-arrow').css('left','75px');
										break;
										case 1:
										jQuery('#business-insurance-megamenu').css('display','block');
										jQuery('#megamenu-arrow').css('display','block');
										jQuery('#megamenu-arrow').css('left','240px');
										break;
										case 2:
										jQuery('#claims-megamenu').css('display','block');
										jQuery('#megamenu-arrow').css('display','block');
										jQuery('#megamenu-arrow').css('left','365px');
										break;
										case 3:
										jQuery('#resource-center-megamenu').css('display','block');
										jQuery('#megamenu-arrow').css('display','block');
										jQuery('#megamenu-arrow').css('left','480px');
										break;
										case 4:
										jQuery('#about-us-megamenu').css('display','block');
										jQuery('#megamenu-arrow').css('display','block');
										jQuery('#megamenu-arrow').css('left','590px');
										break;
									}
								});
							});
						}
						
						if(width < 768) {
							jQuery('#homepage-banner-for-mobile').css('display','');
							jQuery('#homepage-banner-for-big').css('display','none');
						} else {
							jQuery('#homepage-banner-for-mobile').css('display','none');
							jQuery('#homepage-banner-for-big').css('display','');
						}
						
					}
				});
				
				
				
				jQuery("ul#taiping-menu li a").each(function(index) {
					jQuery(this).mouseover(function(){
						jQuery('.megamenu-wrapper').css('display','none');
						jQuery('#megamenu-arrow').css('display','none');
						switch(index) {
							case 0:
							jQuery('#personal-insurance-megamenu').css('display','block');
							jQuery('#megamenu-arrow').css('display','block');
							jQuery('#megamenu-arrow').css('left','75px');
							break;
							case 1:
							jQuery('#business-insurance-megamenu').css('display','block');
							jQuery('#megamenu-arrow').css('display','block');
							jQuery('#megamenu-arrow').css('left','280px');
							break;
							case 2:
							jQuery('#claims-megamenu').css('display','block');
							jQuery('#megamenu-arrow').css('display','block');
							jQuery('#megamenu-arrow').css('left','445px');
							break;
							case 3:
							jQuery('#resource-center-megamenu').css('display','block');
							jQuery('#megamenu-arrow').css('display','block');
							jQuery('#megamenu-arrow').css('left','595px');
							break;
							case 4:
							jQuery('#about-us-megamenu').css('display','block');
							jQuery('#megamenu-arrow').css('display','block');
							jQuery('#megamenu-arrow').css('left','755px');
							break;
						}
					});
					
				});
				/* Tabbing for k2 details - get default active one*/
				var default_text = jQuery('#set-nn_tabs-1 li.active a').html();
				if(default_text) {
					jQuery('#drop-down-nntabs').text(default_text);
					jQuery('#set-nn_tabs-1').css('display','none');
				}
				
				jQuery('#drop-down-nntabs').click(function(){
					jQuery('#set-nn_tabs-1').slideToggle('slow', function() {
						// Animation complete.
					});
					
				});
				jQuery('#set-nn_tabs-1 li a').click(function(){
					var windowsize = jQuery(window).width();
					
					if (windowsize < 768) {
						var text_chosen = jQuery(this).html();
						jQuery('#drop-down-nntabs').text(text_chosen);
						jQuery('#set-nn_tabs-1').css('display','none');
					}
					
				});
				
				var windowsize = jQuery(window).width();

				if (windowsize >= 768) {
					jQuery('#set-nn_tabs-1').css('display','');
				}

				jQuery(window).resize(function() {
					windowsize = jQuery(window).width();
					if (windowsize >= 768) {
						jQuery('#set-nn_tabs-1').css('display','');
					}
					if (windowsize < 768) {
						jQuery('#set-nn_tabs-1').css('display','none');
					}
				});
				
				var viewport = {
					width  : jQuery(window).width(),
					height : jQuery(window).height()
				};
				
				//alert(viewport.width);

				/* Resource Center */
				jQuery('.ars-releases .module').each(function(index){
					if(index % 2 == 0) {
						jQuery(this).css('background-color','#fff');
					}
				});
				
				// megamenu wrapper mouse out
				jQuery('.megamenu-wrapper').mouseleave(function(){
					jQuery(this).css('display','none');
					jQuery('#megamenu-arrow').css('display','none');
					
				});
				
			});
			
			/* for --- Your World - Our Insurance - 2 types only */
			function taipingYourWorldArticle(id) {
				jQuery('.homepage-featured-your-world').css('display','none');
				jQuery('#' + id).css('display','');
				switch(id) {
					case 'hpf-personal':
						jQuery('.tpyourworld-btn').attr('onclick','javascript:taipingYourWorldArticle(\'hpf-business\')');
						break;
					default:
						jQuery('.tpyourworld-btn').attr('onclick','javascript:taipingYourWorldArticle(\'hpf-personal\')');
						break;
				}
			}
			
		</script>
	</head>
	<body id="shadow">
		
		<div class="rt-top">
			<div id="all">
				<jdoc:include type="modules" name="top-menu-2" />
				<div id="back">
					<div id="header">
						<a href="<?php echo JURI::root() ?>">
						<div class="logoheader"><img src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/images/logo.png" /></div><!-- end logoheader -->
						</a>
						<div id="navigation-top">
							<div style="margin-bottom:20px;position:relative;z-index:101;">
								<div style="float:right;margin-right:60px;"><jdoc:include type="modules" name="top-menu-1" /></div>
								<div class="clear"></div>
							</div>
							<div>
								<div style="float:right;"><jdoc:include type="modules" name="top-menu-3" /></div>
								<div style="float:right;margin-right:20px;"><jdoc:include type="modules" name="top-menu-4" /></div>
								<div class="clear"></div>
							</div>
						</div>
						<div class="clear"></div>
					</div><!-- end header -->
				</div><!-- back -->
				
			</div><!-- all -->
		</div>
		
		<div class="rt-mainmenu">
			<div id="all">
				<div id="back" style="position:relative;">
					
					<jdoc:include type="modules" name="taiping-main-menu" />
					
					<!-- megamenu is here -->
					<div id="megamenu-arrow" style="display:none">&nbsp;</div>
					<div class="megamenu-wrapper" id="personal-insurance-megamenu" style="display:none;">
						<div class="megamenu">
							<div class="box1"><jdoc:include type="modules" name="megamenu-personal-1" style="beezStandard" /></div>
							<div class="box2"><jdoc:include type="modules" name="megamenu-personal-2" style="beezStandard" /></div>
							<div class="box3"><jdoc:include type="modules" name="megamenu-personal-3" style="beezStandard" /></div>
							<div class="clear"></div>
						</div>
					</div>
					
					<div class="megamenu-wrapper" id="business-insurance-megamenu" style="display:none;">
						<div class="megamenu">
							<div class="box1b"><jdoc:include type="modules" name="megamenu-business-1" style="beezStandard" /></div>
							<div class="box2b"><jdoc:include type="modules" name="megamenu-business-2" style="beezStandard" /></div>
							<div class="box3b"><jdoc:include type="modules" name="megamenu-business-3" style="beezStandard" /></div>
							<div class="box4b"><jdoc:include type="modules" name="megamenu-business-4" style="beezStandard" /></div>
							<div class="clear"></div>
						</div>
					</div>
					
					<div class="megamenu-wrapper" id="claims-megamenu" style="display:none;">
						<div class="megamenu">
							<div class="box1b"><jdoc:include type="modules" name="megamenu-claims-1" style="beezStandard" /></div>
							<div class="box2b"><jdoc:include type="modules" name="megamenu-claims-2" style="beezStandard" /></div>
							<div class="box3b"><jdoc:include type="modules" name="megamenu-claims-3" style="beezStandard" /></div>
							<div class="box4b"><jdoc:include type="modules" name="megamenu-claims-4" style="beezStandard" /></div>
							<div class="clear"></div>
						</div>
					</div>
					
					<div class="megamenu-wrapper" id="resource-center-megamenu" style="display:none;">
						<div class="megamenu">
							<div class="box1"><jdoc:include type="modules" name="megamenu-resource-1" style="beezStandard" /></div>
							<div class="box2"><jdoc:include type="modules" name="megamenu-resource-2" style="beezStandard" /></div>
							<div class="box3"><jdoc:include type="modules" name="megamenu-resource-3" style="beezStandard" /></div>
							<div class="clear"></div>
						</div>
					</div>
					
					<div class="megamenu-wrapper" id="about-us-megamenu" style="display:none;">
						<div class="megamenu">
							<div class="box1"><jdoc:include type="modules" name="megamenu-aboutus-1" style="beezStandard" /></div>
							<div class="box2"><jdoc:include type="modules" name="megamenu-aboutus-2" style="beezStandard" /></div>
							<div class="box3"><jdoc:include type="modules" name="megamenu-aboutus-3" style="beezStandard" /></div>
							<div class="clear"></div>
						</div>
					</div>
				</div><!-- back -->
				
			</div><!-- all -->
		</div>
		
		<?php if($option == 'com_k2' || $option == 'com_contact' || $option == 'com_ars' || $option == 'com_search') : ?>
		<div class="after-mainmenu-bg">&nbsp;</div>
		<div class="white-grey-bg">
			<div id="all">
				<div id="back">
					<?php if ($this->countModules('tp-breadcrumbs')) : ?>
					<div id="breadcrumbs">
						<jdoc:include type="modules" name="tp-breadcrumbs" />
					</div>
					<?php endif; ?>
					<jdoc:include type="message" />
					<jdoc:include type="component" />
				</div>
			</div>
		</div>
		
		<?php if ($this->countModules('taiping-contact-details')) : ?>
		<div class="white-bg">
			<div id="all"><!-- all - start -->
				<div id="back">
					<jdoc:include type="modules" name="taiping-contact-details" />
				</div>
			</div>
		</div>
		<?php endif; ?>
		
		<?php else : // homepage ?>
		<div id="homepage-banner-for-big">
			<jdoc:include type="modules" name="taiping-homepage-banner" style="beezStandard" />
		</div>
		<div id="homepage-banner-for-mobile">
			<jdoc:include type="modules" name="taiping-homepage-banner-mobile" style="beezStandard" />
		</div>
		<?php if ($this->countModules('taiping-news-headline')) : ?>
		<div class="white-bg">
			<div id="all"><!-- all - start -->
				<div id="back">
					<jdoc:include type="modules" name="taiping-news-headline" style="newsHeadline" />
				</div>
			</div>
		</div>
		<?php endif; ?>
		
		<?php if ($this->countModules('taiping-homepage-your-world') or 
			$this->countModules('taiping-homepage-your-world-personal') or 
			$this->countModules('taiping-homepage-your-world-business') or
		$this->countModules('taiping-homepage-your-world-mobile')) : ?>
		
		<div class="grey-bg1">
			<div id="all2"><!-- all - start -->
				<div id="back" style="position:relative;">
					<div><jdoc:include type="modules" name="taiping-homepage-your-world" style="beezStandard" /></div>
					<div id="hpf-mobile"><jdoc:include type="modules" name="taiping-homepage-your-world-mobile" style="beezStandard" /></div>
					<div id="hpf-personal" class="homepage-featured-your-world" style=""><jdoc:include type="modules" name="taiping-homepage-your-world-personal" style="beezStandard" /></div>
					<div id="hpf-business" class="homepage-featured-your-world" style="display:none;"><jdoc:include type="modules" name="taiping-homepage-your-world-business" style="beezStandard" /></div>
					<a href="javascript:void(0);" onclick="javascript:taipingYourWorldArticle('hpf-business')" id="your-world-left-btn" class="tpyourworld-btn">&nbsp;</a>
					<a href="javascript:void(0);" onclick="javascript:taipingYourWorldArticle('hpf-business')" id="your-world-right-btn" class="tpyourworld-btn">&nbsp;</a>
				</div>
			</div>
		</div>
		
		<?php endif; ?>
		
		<?php if ($this->countModules('taiping-homepage-tabs') or $this->countModules('acymailing-newsletter') or $this->countModules('taiping-search-mobile')) : ?>
		<div class="white-bg">
			<div id="all"><!-- all - start -->
				<div id="back">
					<div id="taiping-homepage-below">
						<jdoc:include type="modules" name="taiping-homepage-tabs" style="beezStandard" />
						<?php if($this->countModules('acymailing-newsletter')) : ?>
						<div class="tp-acymailing">
							<div class="left"><jdoc:include type="modules" name="acymailing-header" style="" /></div>
							<div class="right"><jdoc:include type="modules" name="acymailing-newsletter" style="" /></div>
							<div style="clear:both"></div>
						</div>
						<?php endif; ?>
						<jdoc:include type="modules" name="taiping-search-mobile" style="beezStandard" />
					</div> <!-- end contentarea -->
				</div>
			</div><!-- all - end -->
		</div>
		<?php endif; ?>
		
		<?php endif; ?>
		
		<div id="footer-outer">
			<div id="footer-inner" >
				<div id="bottom">
					<div class="box box1"><jdoc:include type="modules" name="footer-box-1" style="beezStandard" /></div>
					<div class="box box2"><jdoc:include type="modules" name="footer-box-2" style="beezStandard" /></div>
					<div class="box box3"><jdoc:include type="modules" name="footer-box-3" style="beezStandard" /></div>
					<div class="box box4"><jdoc:include type="modules" name="footer-box-4" style="beezStandard2" /></div>
					<div class="box box5"><jdoc:include type="modules" name="footer-box-5" style="beezStandard2" /></div>
					
					<div class="box box2mob"><jdoc:include type="modules" name="footer-box-2-phone" style="beezStandard" /></div>
				</div>
			</div>
			
			<div id="footer-sub">
				<div id="footer">
					<div id="copyright">
						<div class="lefty">
							<div id="copyright-text">&#169; Copyright <?php echo date("Y").' '.$sitename ?></div>
							<div id="copyright-menu"><jdoc:include type="modules" name="copyright-menu" /></div>
							<div class="clear"></div>
							<div id="best-viewed-footer"><jdoc:include type="modules" name="taiping-best-viewed" /></div>
						</div>
						<div class="righty">
							<jdoc:include type="modules" name="copyright-phone" />
						</div>
						<div class="clear"></div>
					</div>
				</div><!-- end footer -->
				
			</div>
		</div>
		
		<jdoc:include type="modules" name="debug" />
        
	</body>
</html>
