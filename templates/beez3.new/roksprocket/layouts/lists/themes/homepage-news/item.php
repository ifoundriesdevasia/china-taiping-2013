<?php
	/**
		* @version   $Id$
		* @author    RocketTheme http://www.rockettheme.com
		* @copyright Copyright (C) 2007 - 2013 RocketTheme, LLC
		* @license   http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
	*/
	
	/**
		* @var $item RokSprocket_Item
	*/
?>

<?php 

$extraFields = array();

if($item->getExtraFields()) : 
	foreach($item->getExtraFields() as $key => $value):
		$extraFields[$key] = $value->value;
	endforeach;
endif; 

?>

<li <?php if (!$parameters->get('lists_enable_accordion') || $index == 0): ?>class="active" <?php endif;?>data-lists-item>
	
	<span class="sprocket-lists-item" data-lists-content>
		<span class="sprocket-padding">
			<?php if ($item->getPrimaryImage()) :?>
			<img src="<?php echo $item->getPrimaryImage()->getSource(); ?>" class="sprocket-lists-image" />
			<?php endif; ?>
			
			<div class="sprocket-pdg">
			
			<?php /* dates */ ?>
			<div class="sprocket-dates">
				<?php echo date("jS M Y",strtotime($extraFields[0])); ?>
			</div>&nbsp;&nbsp;
		
			<?php if ($item->custom_can_show_title): ?>
			<h4 class="sprocket-lists-title<?php if ($parameters->get('lists_enable_accordion')): ?> padding<?php endif; ?>" data-lists-toggler>
				
				<?php if (!empty($extraFields[1][1])) : ?>
				<a href="<?php echo $extraFields[1][1] ?>" <?php echo ($extraFields[1][2] == 'new')?' target="_blank" ':'';?> >
				<?php endif; ?>
				
				<?php echo $item->getTitle();?>
				
				<?php if (!empty($extraFields[1][1])) : ?>
				</a>
				<?php endif; ?>
				
				<?php if ($parameters->get('lists_enable_accordion')): ?><span class="indicator"><span>+</span></span><?php endif; ?>
				
			</h4>
			<?php endif; ?>
			
			<?php echo $item->getText(); ?>
			
			<?php if ($item->getPrimaryLink()) : ?>
			<a href="<?php echo $item->getPrimaryLink()->getUrl(); ?>" class="readon"><span><?php rc_e('READ_MORE'); ?></span></a>
			<?php endif; ?>
			</div>
		</span>
	</span>
</li>
