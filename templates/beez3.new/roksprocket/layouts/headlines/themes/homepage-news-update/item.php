<?php
/**
 * @version   $Id$
 * @author    RocketTheme http://www.rockettheme.com
 * @copyright Copyright (C) 2007 - 2013 RocketTheme, LLC
 * @license   http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 only
 */

/**
 * @var $item RokSprocket_Item
 */
?>

<?php 

$extraFields = array();

if($item->getExtraFields()) : 
	foreach($item->getExtraFields() as $key => $value):
		$extraFields[$key] = $value->value;
	endforeach;
endif; 

?>
<li>
	<span class="sprocket-headlines-item<?php echo (!$index) ? ' active' : ''; ?>" data-headlines-item>
		<?php if ( $item->getPrimaryImage()) :?>
		<img src="<?php echo $item->getPrimaryImage()->getSource(); ?>" class="sprocket-headlines-image" />
		<?php endif; ?>
		
		<?php /* dates */ ?>
		<span class="sprocket-dates">
			<?php echo date("d.m.Y",strtotime($extraFields[0])); ?>
		</span>&nbsp;&nbsp;
		
		<?php if (!empty($extraFields[1][1])) : ?>
		<a href="<?php echo $extraFields[1][1] ?>" class="sprocket-headlines-text" <?php echo ($extraFields[1][2] == 'new')?' target="_blank" ':'';?> >
		<?php else : ?>
		<span class="sprocket-headlines-text">
		<?php endif; ?>
		
			<?php echo $item->getText(); ?>
			
			
		<?php if (!empty($extraFields[1][1])) : ?>
		</a>
		<?php else : ?>
		</span>
		<?php endif; ?>
		
		
		&nbsp;&nbsp;&nbsp;
		<?php if(!empty($extraFields[1])) : ?>
		<a style="text-decoration:underline" href="<?php echo $extraFields[1][1] ?>" <?php echo ($extraFields[1][2] == 'new')?' target="_blank" ':'';?>>
		<?php echo $extraFields[1][0] ?>
		</a>
		<?php endif; ?>
	</span>
</li>
