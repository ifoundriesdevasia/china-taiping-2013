<?php
/**
 * @package   AkeebaReleaseSystem
 * @copyright Copyright (c)2010-2018 Nicholas K. Dionysopoulos / Akeeba Ltd
 * @license   GNU General Public License version 3, or later
 */

defined('_JEXEC') or die;

/** @var  \Akeeba\ReleaseSystem\Site\View\Releases\Html  $this */
?>

<div class="item-page<?php echo $this->escape($this->params->get('pageclass_sfx')); ?>">
	<?php if($this->params->get('show_page_heading')): ?>
	<div class="page-header">
		<h1>
			<?php echo $this->escape($this->params->get('page_heading', $this->menu->title)); ?>

		</h1>
	</div>
	<?php endif; ?>

	<?php echo $this->loadAnyTemplate('site:com_ars/Releases/category', ['id' => $this->category->id, 'item' => $this->category, 'Itemid' => $this->Itemid, 'no_link' => true]); ?>

	<div class="ars-releases ars-releases-<?php echo $this->category->is_supported ? 'supported' : 'unsupported'; ?>">
	<?php if(count($this->items)): ?>
		<?php foreach($this->items as $item): ?>
				<?php echo $this->loadAnyTemplate('site:com_ars/Releases/release', ['item' => $item, 'Itemid' => $this->Itemid]); ?>
		<?php endforeach; ?>
	<?php else: ?>
		<div class="ars-noitems">
			<?php echo \JText::_('ARS_NO_RELEASES'); ?>
		</div>
	<?php endif; ?>
	</div>
</div>
