<?php
/**
 * @package   AkeebaReleaseSystem
 * @copyright Copyright (c)2010-2018 Nicholas K. Dionysopoulos / Akeeba Ltd
 * @license   GNU General Public License version 3, or later
 */

defined('_JEXEC') or die;

/** @var  \Akeeba\ReleaseSystem\Site\View\Items\Html  $this */

$released = $this->container->platform->getDate($this->release->created);
?>

<div class="item-page<?php echo $this->escape($this->params->get('pageclass_sfx')); ?> akeeba-bootstrap">
	<?php if($this->params->get('show_page_heading')): ?>
	<div class="page-header">
		<h2 class="componentheading">
			<?php echo $this->escape($this->params->get('page_heading', $this->menu->title)); ?>

		</h2>
	</div>
	<?php endif; ?>
	
	{modulepos resource-center-menu}

	<?php echo $this->loadAnyTemplate('site:com_ars/Items/release', ['id' => $this->release->id, 'item' => $this->release, 'Itemid' => $this->Itemid, 'no_link' => true]); ?>

	<div class="ars-releases ars-items ars-items-<?php echo $this->release->category->is_supported ? 'supported' : 'unsupported'; ?>">
	<?php if(count($this->items)): ?>
		<?php foreach($this->items as $item): ?>
			<?php echo $this->loadAnyTemplate('site:com_ars/Items/item', ['item' => $item, 'Itemid' => $this->Itemid]); ?>
		<?php endforeach; ?>
	<?php else: ?>
		<div class="ars-noitems">
			<?php echo \JText::_('ARS_NO_ITEMS'); ?>
		</div>
	<?php endif; ?>
	</div>
</div>
