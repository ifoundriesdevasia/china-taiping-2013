<?php
/**
 * @package   AkeebaReleaseSystem
 * @copyright Copyright (c)2010-2018 Nicholas K. Dionysopoulos / Akeeba Ltd
 * @license   GNU General Public License version 3, or later
 */

defined('_JEXEC') or die;

/** @var  \Akeeba\ReleaseSystem\Site\View\Items\Html $this */

use Akeeba\ReleaseSystem\Site\Helper\Filter;
use Akeeba\ReleaseSystem\Site\Helper\Router;
use Akeeba\ReleaseSystem\Admin\Helper\Format;
use Akeeba\ReleaseSystem\Admin\Helper\Select;

$download_url =
		Router::_('index.php?option=com_ars&view=Item&task=download&format=raw&id=' . $item->id . '&Itemid=' . $this->Itemid);

if (!Filter::filterItem($item, false, $this->getContainer()->platform->getUser()->getAuthorisedViewLevels()) && !empty($item->redirect_unauth))
{
	$download_url = $item->redirect_unauth;
}

$directLink = false;

if ($this->directlink)
{
	$basename = ($item->type == 'file') ? $item->filename : $item->url;

	foreach ($this->directlink_extensions as $ext)
	{
		if (substr($basename, -strlen($ext)) == $ext)
		{
			$directLink = true;
			break;
		}
	}

	if ($directLink)
	{
		$directLinkURL = $download_url .
				(strstr($download_url, '?') !== false ? '&' : '?') .
				'dlid=' . $this->downloadId . '&jcompat=my' . $ext;
	}
}

if (!Filter::filterItem($item, false, $this->getContainer()->platform->getUser()->getAuthorisedViewLevels()) && !empty($item->redirect_unauth))
{
	$download_url = $item->redirect_unauth;
	$directLink = false;
}

$js = <<<JS
if (typeof(akeeba) == 'undefined')
{
	var akeeba = {};
}

if (typeof(akeeba.jQuery) === 'undefined')
{
	akeeba.jQuery = window.jQuery;
}

akeeba.jQuery(document).ready(function($){
    akeeba.fef.tabs();

    $('.release-info-toggler').off().on('click', function(){
        var target = $(this).data('target');
        $(target).slideToggle();
    })
});
JS;

$this->getContainer()->template->addJSInline($js);
?>

<div class="ars-item-<?php echo $this->escape($item->id); ?> module" style="background-color: rgb(255, 255, 255);">	
	<h3>
		<a href="<?php echo htmlentities($download_url); ?>">
			<?php echo $this->escape($item->title); ?>

		</a>
	</h3>
	<div class="custom">
		<div class="ars-browse-items">
			<div class="ars-item-properties">				
				
				<?php if ( ! (empty($item->filesize) || !$this->params->get('show_filesize',1))): ?>
					<span class="ars-release-property">
						<span class="ars-label"><?php echo \JText::_('LBL_ITEMS_FILESIZE'); ?></span>
						<span class="ars-value"><?php echo Format::sizeFormat($item->filesize); ?></span>
					</span>
				<?php endif; ?>
				
				<?php if ( ! (empty($item->md5) || !$this->params->get('show_md5',1))): ?>
					<span class="ars-release-property">
						<span class="ars-label"><?php echo \JText::_('LBL_ITEMS_MD5'); ?></span>
						<span class="ars-value"><?php echo $this->escape($item->md5); ?></span>
					</span>
				<?php endif; ?>
				
				<?php if ( ! (empty($item->sha1) || !$this->params->get('show_sha1',1))): ?>
					<span class="ars-release-property">
						<span class="ars-label"><?php echo \JText::_('LBL_ITEMS_SHA1'); ?></span>
						<span class="ars-value"><?php echo $this->escape($item->sha1); ?></span>
					</span>
				<?php endif; ?>
				
				<?php if ( ! (empty($item->environments) || !$this->params->get('show_environments',1))): ?>
					<span class="ars-release-property">
						<span class="ars-label"><?php echo \JText::_('LBL_ITEMS_ENVIRONMENTS'); ?></span>
						<span class="ars-value">
							<?php foreach($item->environments as $environment): ?>
								<?php echo Select::environmentIcon($environment); ?>

							<?php endforeach; ?>
						</span>
					</span>
				<?php endif; ?>
			</div>		
			
			<?php if ( ! (empty($item->description))): ?>
				<div class="ars-item-description">
					<?php echo Format::preProcessMessage($item->description, 'com_ars.item_description'); ?>
				</div>
			<?php endif; ?>
			
			<div>
				<a href="<?php echo htmlentities($download_url); ?>" class="akeeba-btn--primary readon">
					<?php echo \JText::_('LBL_ITEM_DOWNLOAD'); ?>
				</a>

				<?php if ( ! (!$directLink)): ?>
					<a rel="nofollow" href="<?php echo htmlentities($directLinkURL); ?>"
					   class="directlink hasTip readon" title="<?php echo $this->escape($this->directlink_description); ?>">
						<?php echo \JText::_('COM_ARS_LBL_ITEM_DIRECTLINK'); ?>
					</a>
				<?php endif; ?>
			</div>		
		</div>
	</div>
	<div style="clear:both"></div>
</div>
